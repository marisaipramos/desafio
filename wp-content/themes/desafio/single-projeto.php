<?php get_header(); ?>

    <div class="container pt-5 pb-5">
        <div class="row">
            <div class="col">

                <?php if (have_posts()): while (have_posts()):the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <?php the_title('<h1 class="entry-title">', '</h1>'); ?>

                        <?php if (has_post_thumbnail()): ?>
                            <div><?php the_post_thumbnail('thumbnail'); ?></div>
                        <?php endif; ?>

                        <?php the_content(); ?>

                        <?php previous_post_link('%link', '⮜ Anterior', true); ?>
                        <?php next_post_link('%link', 'Seguinte ⮞', true); ?>

                    </article>
                <?php endwhile; endif; ?>

            </div>
        </div>
    </div>

<?php get_footer(); ?>