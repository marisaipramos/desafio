<?php

function load_stylesheets()
{
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css',
        [], false, 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('style', get_template_directory_uri() . '/style.css',
        [], false, 'all');
    wp_enqueue_style('style');
}

add_action('wp_enqueue_scripts', 'load_stylesheets');


function include_jquery()
{
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-3.1.3.min.js', '', 1, true);

    add_action('wp_enqueue_scripts', 'jquery');
}

add_action('wp_enqueue_scripts', 'include_jquery');


function loadjs()
{
    wp_register_script('customjs', get_template_directory_uri() . '/js/scripts.js', '', 1, true);
    wp_enqueue_script('customjs');
}

add_action('wp_enqueue_scripts', 'loadjs');


add_theme_support('menus');
add_theme_support('post-thumbnails');

register_nav_menus(
    [
        'top-menu'    => __('Top menu', 'theme'),
        'footer-menu' => __('Footer menu', 'theme'),
    ]
);

function add_class_next_post_link($html)
{
    $html = str_replace('<a', '<a class="btn btn-dark"', $html);

    return $html;
}

add_filter('next_post_link', 'add_class_next_post_link', 10, 1);

function add_class_previous_post_link($html)
{
    $html = str_replace('<a', '<a class="btn btn-dark"', $html);

    return $html;
}

add_filter('previous_post_link', 'add_class_previous_post_link', 10, 1);

/*--------------------------------------------------*/
/*            Custom Post Type - Projeto            */
/*--------------------------------------------------*/
function projeto_custom_post_type()
{
    $labels = [
        'name'               => 'Projeto',
        'singular_name'      => 'Projeto',
        'add_new'            => 'Novo projeto',
        'all_items'          => 'Todos os projetos',
        'add_new_item'       => 'Add Item',
        'edit_item'          => 'Edit Item',
        'new_item'           => 'New Item',
        'view_item'          => 'View Item',
        'search_item'        => 'Search Projeto',
        'not_found'          => 'No items found',
        'not_found_in_trash' => 'No items found in trash',
        'parent_item_colon'  => 'Parent Item',
    ];

    $args = [
        'labels'              => $labels,
        'public'              => true,
        'has_archive'         => true,
        'publicly_queryable'  => true,
        'query_var'           => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        'hierarchical'        => false,
        'support'             => [
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'revisions',
        ],
        'taxonomies'          => ['category', 'post_tag'],
        'menu_position'       => 5,
        'exclude_from_search' => false,
    ];
    register_post_type('projeto', $args);

}

add_action('init', projeto_custom_post_type);
