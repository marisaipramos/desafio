<?php
/**
 * Template Name: Projeto
 */

get_header(); ?>

    <div class="container pt-5 pb-5">
        <div class="row">
            <div class="col">

                <h1><?php single_cat_title(); ?></h1>

                <!--Show Post Type-->
                <?php
                $args = ['post_type' => 'projeto', 'post_per_page' => 3];
                $loop = new WP_Query($args);
                ?>

                <?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
                    <div class="card mb-4">
                        <div class="card-body">

                            <?php if (has_post_thumbnail()): ?>
                                <img src="<?php the_post_thumbnail_url('medium'); ?>" class="img-fluid">
                            <?php endif; ?>

                            <h3><?php the_title(); ?></h3>
                            <?php the_excerpt(); ?>
                            <a href="<?php the_permalink(); ?>" class="btn btn-dark">Ler mais</a>

                        </div>
                    </div>

                <?php endwhile; endif; ?>

                <!--Show Link YouTube-->
                <div>
                    <?php
                    $meta_youtube = get_post_meta(9, 'youtube', true);
                    echo '<a href="' . $meta_youtube . '" target="_blank">Ir para o video ⮞</a>';
                    ?>
                </div>

                <!--Show Image-->
                <div>
                    <?php
                    $meta_image = get_post_meta(9, 'image', true);
                    echo '<img src="' . $meta_image . '" alt="">';
                    ?>
                </div>

            </div>
        </div>
    </div>

<?php get_footer(); ?>